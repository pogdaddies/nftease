npm install --save-dev hardhat
npm install dotenv --save
npm install --save-dev @nomiclabs/hardhat-ethers ethers@^5.0.0
npm install @openzeppelin/contracts
npx hardhat compile
npx hardhat run scripts/deploy.js --network ropsten